//
//  detailNewsPaper.h
//  bookstore
//
//  Created by Herman Tolle on 7/16/14.
//  Copyright (c) 2014 Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface detailNewsPaper : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) NSString *judulvar;
@property (weak, nonatomic) IBOutlet UILabel *judul;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end
